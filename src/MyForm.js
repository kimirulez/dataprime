import React from 'react';
import { Button } from '@material-ui/core';
import Popup from "reactjs-popup";

function MyForm() {
    return (
      <Popup trigger={<Button variant="contained" color="primary"> Contact Us
                      </Button>} position="left center">
        <form>
          <h1>Contact Us</h1>
          <label for="contact">Contact Type</label>
          <div style={{paddingTop:20}}>
            <select name="contact" id="contacts">
              <option value="Email">Email</option>
              <option value="Phone">Phone</option>
              <option value="Messenger">Messenger</option>
              <option value="Twitter">Twitter</option>
            </select>
          </div>
          <div style={{paddingTop:20}}>
          Type Message
          </div>
          <div style={{paddingTop:20}}>
            <input
              type="text"
            />
          </div>
          <div style={{paddingTop:20}}>
          <Button variant="contained" color="primary"> Submit </Button>
          <Button variant="contained" color="secondary"> Cancel </Button>
          </div>
        </form>
      </Popup>
    );
  }

export default MyForm;
