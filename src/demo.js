import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Button } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import { CircularProgressbar, buildStyles  } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import MyForm from "./MyForm";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(skill, candidate, vs, business) {
  return { skill, candidate, vs, business };
}

const rows = [
  createData('Machine Learning',1,25,4),
  createData('AWS', 1,50,2),
  createData('MySQL', 1,33,3),
  createData('MongoDB', 1,300,1),
  createData('TensorFlow', 1,20,5),
];

export default function Demo() {
  const classes = useStyles();

  return (
    <div> GENENTECH - Manufacturing Data Scientist
    <div>
      <div style={{padding:20,float:'left'}}>
        <Button variant="contained" disabled>
          Aditya Agarwal
        </Button>
      </div>
      <div style={{padding:20,float:'right'}}>
        <MyForm />
      </div>
    </div>
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table" style={{backgroundColor:'#c7c7c7', color: 'white',}}>
        <TableHead style={{backgroundColor:'#999999',}}>
          <TableRow>
            <TableCell>Technical Skills</TableCell>
            <TableCell align="right">Candidate</TableCell>
            <TableCell align="right">VS(%)</TableCell>
            <TableCell align="right">Business</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.name}>
              <TableCell component="th" scope="row">
                {row.skill}
              </TableCell>
              <TableCell align="right" width="50px">
                <CircularProgressbar  value={row.candidate}
                                      maxValue={1}
                                      text={`${row.candidate}`}
                                      background
                                      styles={buildStyles({
                                                backgroundColor: "black",
                                                textColor: "#fff",
                                                pathColor: "white",
                                                trailColor: "transparent"
                                              })} /></TableCell>
              <TableCell align="right" width="50px">
                <CircularProgressbar  value={row.vs}
                                      maxValue={100}
                                      text={`${row.vs}%`}
                                      background
                                      styles={buildStyles({
                                                backgroundColor: "#B30B00",
                                                textColor: "#fff",
                                                pathColor: "#e6ecff",
                                                trailColor: "transparent"
                                              })} /></TableCell>
              <TableCell align="right" width="50px">
                <CircularProgressbar  value={row.business}
                                      maxValue={5}
                                      text={`${row.business}`}
                                      background
                                      styles={buildStyles({
                                                backgroundColor: "black",
                                                textColor: "#fff",
                                                pathColor: "white",
                                                trailColor: "transparent"
                                              })} /></TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    </div>
  );
}
